<div class="wrap">
    <h1>VenueTown Importer Settings</h1>
    <?php
    if ($updated) {
        echo '<div class="updated">';
        echo '<p>Settings Saved!</p>';
        echo '</div>';
    }
    ?>
    <form method="post">
        <table class="form-table">
            <tbody>                
                <tr>
                    <th scope="row"><label for="vti_venue_user_id">Venue User ID:</label></th>
                    <td>
                        <input type="text" name="vti_venue_user_id" id="vti_venue_user_id" value="<?php echo get_option('vti_venue_user_id'); ?>">
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="vti_planner_user_id">Planner User ID:</label></th>
                    <td>
                        <input type="text" name="vti_planner_user_id" id="vti_planner_user_id" value="<?php echo get_option('vti_planner_user_id'); ?>">
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="vti_save_settings" id="vti_save_settings" class="button button-primary" value="Save"></p>
    </form>
</div>