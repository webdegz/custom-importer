<div class="wrap">
    <h1>VenueTown Importer</h1>
    <?php
    if ($imported) {
        echo '<div class="updated">';
        echo '<p><b>Import file successfully:</b> Report - Success: '.$this->success_count.' | Error: '.$this->error_count.' | Skip:'.$this->skip_count;
        echo '</div>';
    }
    ?>
    <form method="post" enctype="multipart/form-data">
        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row"><label for="vti_spreadsheet">Browse Spreadsheet:</label></th>
                    <td>
                        <input type="file" name="vti_spreadsheet" id="vti_spreadsheet" required="required">
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="vti_import_for">Import For:</label></th>
                    <td>
                        <select name="vti_import_for" id="vti_import_for" required="required">
                            <option value="">Select Import For</option>
                            <option value="venues">Venues</option>
                            <option value="planners">Planners</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="vti_import_for">Type:</label></th>
                    <td>
                        <select name="vti_type" id="vti_type" required="required">
                            <option value="">Select Type</option>
                            <option value="data">Data</option>
                            <option value="images">Images</option>
                            <option value="attachments">Attachments</option>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <p class="submit"><input type="submit" name="vti_import" id="vti_import" class="button button-primary" value="Import"></p>
    </form>
</div>