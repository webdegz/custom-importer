<?php

class VenueTown_Importer {

    public $venue = array();
    public $planner = array();
    private $done_post = array();
    private $image_gallery = array();
    private $attachments = array();
    private $floor_plans = array();
    private $success_count = 0;
    private $error_count = -1;
    private $skip_count = 0;
    private $countries = array();
    private $states = array();

    /**
     * Constructor
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'vti_importer_menu'));
        include('config/param.php');
        include('config/countries-states.php');        
    }

    /**
     * Register Menu
     */
    public function vti_importer_menu() {
        add_menu_page("VenueTown Importer", "VenueTown Importer", 'administrator', 'vti_importer', array($this, 'vti_importer_page'));
        add_options_page('Importer Settings', 'VenueTown Importer Settings', 'manage_options', 'vti-settings', array($this, 'vti_setting_page'));
    }

    /**
     * Setting Page Content
     */
    public function vti_setting_page() {
        if (isset($_POST['vti_save_settings'])) {
            update_option('vti_venue_user_id', $_POST['vti_venue_user_id']);
            update_option('vti_planner_user_id', $_POST['vti_planner_user_id']);
        }
        include('templates/settings.php');
    }

    /**
     * Importer Page Content
     */
    public function vti_importer_page() {
        if (isset($_POST['vti_import'])) {
            if (isset($_FILES["vti_spreadsheet"])) {
                $spreadsheet = fopen($_FILES["vti_spreadsheet"]["tmp_name"], "r");
                $count = 0;
                while (!feof($spreadsheet)) {
                    $data = fgetcsv($spreadsheet);
                    if ($count <> 0) {

                        $response = $this->vti_insert_post($data, $_POST['vti_import_for'], $_POST['vti_type']);

                        if ($response['status'] == 'success') {
                            $this->success_count++;
                        } elseif ($response['status'] == 'skip') {
                            $this->skip_count++;
                        } else {
                            $this->error_count++;
                        }
                    }
                    $count++;
                }

                /**
                 * Update post gallery images
                 */
                if ($_POST['vti_type'] == 'images') {
                    $this->vti_add_gallery($_POST['vti_import_for']);
                }

                /**
                 * Add post attachments and floor plans
                 */
                if ($_POST['vti_type'] == 'attachments') {
                    $this->vti_add_attachments($_POST['vti_import_for']);
                    $this->vti_add_floor_plans($_POST['vti_import_for']);
                }

                $imported = true;
            }
        }
        include('templates/importer.php');
    }

    /**
     * Add Gallery
     * @param String $action
     */
    public function vti_add_gallery($action) {
        $meta_key = null;

        switch ($action) {
            case 'venues':
                $meta_key = "image_gallery";
                break;
            case 'planners':
                $meta_key = "attachment_pictures";
                break;
        }

        if ($meta_key) {
            foreach ($this->image_gallery as $post_id => $gallery) {
                update_post_meta($post_id, $meta_key, $gallery);
            }
        }
        $this->image_gallery = array();
    }

    /**
     * Add Attachments
     * @param String $action
     */
    public function vti_add_attachments($action) {
        $meta_key = null;

        switch ($action) {
            case 'venues':
                $meta_key = "packages_attachment";
                break;
        }

        if ($meta_key) {
            foreach ($this->attachments as $post_id => $attachment) {
                update_post_meta($post_id, $meta_key, $attachment);
            }
        }

        $this->attachments = array();
    }

    /**
     * Add Floor Plans
     * @param String $action
     */
    public function vti_add_floor_plans($action) {
        $meta_key = null;

        switch ($action) {
            case 'venues':
                $meta_key = "docs_attachment";
                break;
        }

        if ($meta_key) {
            foreach ($this->floor_plans as $post_id => $floor_plan) {
                update_post_meta($post_id, $meta_key, $floor_plan);
            }
        }
        $this->floor_plans = array();
    }

    /**
     * Insert Venues and Planners from Data, Images & Additional materials Sheets    
     * @param Array $data
     * @param String $action
     * @param String $type
     * @return boolean
     */
    public function vti_insert_post($data, $action, $type) {
        switch ($action) {
            case 'venues':
                $data_fields = $this->venue['data'];
                $images_fields = $this->venue['images'];
                $attachments_fields = $this->venue['attachments'];
                $data_fields_required = $this->venue['data_required'];
                $images_fields_required = $this->venue['images_required'];
                $attachment_fields_required = $this->venue['attachment_required'];
                $tags = $this->venue['venue_tags'];
                $author = get_option('vti_venue_user_id');
                $post_type = "venue";
                $taxonomy = "venue_category";
                $tags_taxonomy = "venue_tags_specializations";
                $language_taxonomy = "venue_language";
                $country_language = $this->venue['country_language'];
                $user_info = get_userdata(get_option('vti_venue_user_id'));
                break;
            case 'planners':
                $data_fields = $this->planner['data'];
                $images_fields = $this->planner['images'];
                $data_fields_required = $this->planner['data_required'];
                $images_fields_required = $this->planner['images_required'];
                $author = get_option('vti_planner_user_id');
                $post_type = "planner";
                $taxonomy = "planner_category";
                $language_taxonomy = "planner_language";
                $country_language = $this->planner['country_language'];
                $user_info = get_userdata(get_option('vti_planner_user_id'));
                break;
        }
        
        foreach((array)$data as $key => $post_data){
            $data[$key] = trim($post_data);
        }
        
        switch ($type) {
            case 'data':                
                if ($this->check_required_fields($data, $data_fields_required, $data_fields)) {
                    return $this->vti_insert_data($data, $data_fields, $author, $post_type, $taxonomy, $tags_taxonomy, $language_taxonomy, $tags);
                } else {                    
                    return false;
                }
                break;
            case 'images':
                if ($this->check_required_fields($data, $images_fields_required, $images_fields)) {
                    return $this->vti_attach_images($data, $images_fields, $post_type, $user_info);
                } else {
                    return false;
                }
                break;
            case 'attachments':
                if ($this->check_required_fields($data, $attachment_fields_required, $attachments_fields)) {
                    return $this->vti_attachment($data, $attachments_fields, $post_type, $user_info);
                } else {
                    return false;
                }
                break;
        }
    }

    /**
     * Validate required fields
     * @param Array $data
     * @param Array $fields_required
     * @param Array $fields
     * @return boolean
     */
    public function check_required_fields($data, $fields_required, $fields) {
        
        $return = true;
        
        foreach ($fields_required as $required) {            
            if (empty($data[$fields[$required]])) {                
                $return = false;
                break;
            }
        }
        
        return $return;
    }

    /**
     * Get post by import id
     * @param String $post_type
     * @param String $key
     * @param String $value
     * @return Object
     */
    public function vti_get_post_by_import_id($post_type, $key, $value) {
        $query = new WP_Query(array(
            'post_type' => $post_type,
            'meta_key' => $key,
            'meta_value' => $value)
        );

        if ($query->have_posts()) {
            return $query->posts[0];
        } else {
            return false;
        }
    }

    /**
     * 
     * @param Array $data
     * @param Array $fields
     * @param String $post_type
     * @param Object $user_info
     */
    public function vti_attachment($data, $fields, $post_type, $user_info) {
        if ($post = $this->vti_get_post_by_import_id($post_type, 'vti_import_id', $data[$fields['vti_import_id']])) {

            //Check duplicate attachments
            $post_attachments = get_post_meta($post->ID, 'packages_attachment', true);
            if (is_array($post_attachments)) {
                foreach ($post_attachments as $post_attachment_id => $post_attachment) {
                    if (basename($post_attachment) == $data[$fields['file_name']]) {
                        $this->attachments[$post->ID][$post_attachment_id] = $post_attachment;
                        return array('status' => 'error');
                    }
                }
            }

            //Check duplicate floor plan
            $post_attachments = get_post_meta($post->ID, 'docs_attachment', true);
            if (is_array($post_attachments)) {
                foreach ($post_attachments as $post_attachment_id => $post_attachment) {
                    if (basename($post_attachment) == $data[$fields['file_name']]) {
                        $this->attachments[$post->ID][$post_attachment_id] = $post_attachment;
                        return array('status' => 'error');
                    }
                }
            }

            $attachment = $this->vti_insert_media($data, $fields, $post_type, $user_info);
            if (strtolower($data[$fields['title']]) == "floor plans" || strtolower($data[$fields['title']]) == "floor_plans") {
                $this->floor_plans[$post->ID][$attachment['attach_id']] = $attachment['url'];
            } else {
                $this->attachments[$post->ID][$attachment['attach_id']] = $attachment['url'];
            }
            return array('status' => 'success');
            ;
        }
    }

    /**
     * 
     * @param Array $data
     * @param Array $fields
     * @param String $post_type
     * @param Object $user_info
     * @return boolean
     */
    public function vti_attach_images($data, $fields, $post_type, $user_info) {

        if ($post = $this->vti_get_post_by_import_id($post_type, 'vti_import_id', $data[$fields['vti_import_id']])) {

            if ($_POST['vti_import_for'] == "venues") {
                $meta_key = "image_gallery";
            } elseif ($_POST['vti_import_for'] == "planners") {
                $meta_key = "attachment_pictures";
            }

            //Duplicate featured image
            if (has_post_thumbnail($post->ID)) {
                $featured_image_url = get_the_post_thumbnail_url($post->ID, 'full');
                if (basename($featured_image_url) == $data[$fields['file_name']]) {
                    return array('status' => 'error');
                }
            }

            //Check duplicate gallery
            $post_gallery = get_post_meta($post->ID, $meta_key, true);
            if (is_array($post_gallery)) {
                foreach ($post_gallery as $post_gallery_id => $image_url) {
                    if (basename($image_url) == $data[$fields['file_name']]) {
                        $this->attachments[$post->ID][$post_gallery_id] = $image_url;
                        return array('status' => 'error');
                    }
                }
            }

            $attachment = $this->vti_insert_media($data, $fields, $post_type, $user_info);

            if (!in_array($post->ID, $this->done_post)) {
                set_post_thumbnail($post->ID, $attachment['attach_id']);
            } else {
                $this->image_gallery[$post->ID][$attachment['attach_id']] = $attachment['url'];
            }

            array_push($this->done_post, $post->ID);

            return array('status' => 'success');
        }
    }

    /**
     * Insert image in media library
     * Set custom route for media
     * @param Array $data
     * @param Array $fields
     * @param String $post_type
     * @param Object $user_info
     * @return Array
     */
    public function vti_insert_media($data, $fields, $post_type, $user_info) {
        $media_path = get_home_path() . 'media/' . $post_type . '/' . $data[$fields['file_name']];
        $upload_directory = wp_upload_dir(date('Y/m'));
        $filename = $upload_directory['path'] . '/' . $user_info->user_login . '/' . $post_type . '/' . $data[$fields['file_name']];
        $file_url = $upload_directory['url'] . '/' . $user_info->user_login . '/' . $post_type . '/' . $data[$fields['file_name']];

        if (file_exists($media_path)) {
            error_reporting(E_ERROR | E_PARSE);
            if (copy($media_path, $filename)) {

                // Check the type of file. We'll use this as the 'post_mime_type'.
                $filetype = wp_check_filetype(basename($filename), null);

                // Get the path to the upload directory.
                $wp_upload_dir = wp_upload_dir();

                if (!$data[$fields['title']]) {
                    $title = preg_replace('/\.[^.]+$/', '', basename($file_url));
                } else {
                    $title = $data[$fields['title']];
                }

                // Prepare an array of post data for the attachment.
                $attachment = array(
                    'guid' => $file_url,
                    'post_mime_type' => $filetype['type'],
                    'post_title' => $title,
                    'post_content' => '',
                    'post_status' => 'inherit'
                );

                // Insert the attachment.
                $attach_id = wp_insert_attachment($attachment, $filename);

                // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
                require_once( ABSPATH . 'wp-admin/includes/image.php' );

                // Generate the metadata for the attachment, and update the database record.
                $attach_data = wp_generate_attachment_metadata($attach_id, $filename);
                wp_update_attachment_metadata($attach_id, $attach_data);

                return array('attach_id' => $attach_id, 'url' => $file_url);
            }
        }
    }

    /**
     * Insert post, categories and meta fields from data sheet
     * @param Array $data
     * @param Array $fields
     * @param String $author
     * @param String $post_type
     * @param String $taxonomy
     * @return boolean
     */
    public function vti_insert_data($data, $fields, $author = null, $post_type = null, $taxonomy = null, $tags_taxonomy = null, $language_taxonomy = null, $tags) {

        if (!$this->vti_get_post_by_import_id($post_type, 'vti_import_id', $data[$fields['vti_import_id']])) {

            if ($post_id = $this->vti_insert_wp_post_category($data, $fields, $author, $post_type, $taxonomy, $tags_taxonomy, $language_taxonomy, $tags)) {

                //Unset Post Data
                unset($data['title']);
                unset($data['content']);
                unset($data['excerpt']);

                $formated_data = $this->vti_format_meta_fields($data, $fields);

                foreach ($fields as $field_name => $field) {
                    if ($formated_data[$field]) {
                        if ($field_name == "days_open") {
                            foreach ($formated_data[$field] as $day) {
                                add_post_meta($post_id, $field_name, strtolower($day));
                            }
                        } else {
                            add_post_meta($post_id, $field_name, $formated_data[$field]);
                        }
                    }
                }
                return array('status' => 'success');
            } else {
                return array('status' => 'failed');
            }
        } else {
            return array('status' => 'skip');
        }
    }

    /**
     * Format meta fields 
     * @param Array $data
     * @param Array $fields
     * @return Array
     */
    public function vti_format_meta_fields($data, $fields) {
        foreach ($fields as $field_name => $field) {
            
            if($field_name == "country"){                
                if ($data[$field]) {
                    if($country_data = $this->vti_get_country($data[$field])){
                        $data[$field] = $country_data['country_name'];
                        $data['country_code'] = $country_data['country_code'];
                    }else{
                        $data[$field] = $data[$field];
                    }
                }
            }
            
            if($field_name == "state"){                                
                if ($data[$field]) {
                    if($data['country_code']){
                        $data[$field] = $this->vti_get_state($data['country_code'], $data[$field]);                 
                    }
                }
            }
            
            if ($field_name == "days_open") {
                if ($data[$field]) {
                    $data[$field] = $this->vti_get_open_days($data[$field]);
                }
            }

            if ($field_name == "wi_fi_capability") {
                if ($data[$field]) {
                    $data[$field] = "yes";
                } else {
                    $data[44] = "no";
                }
            }

            if ($field_name == "outside_suppliers_permitted") {
                if ($data[$field]) {
                    $data[$field] = "yes";
                } else {
                    $data[91] = "no";
                }
            }
        }

        return $data;
    }
    
    /**
     * Get states
     * @param string $country_code
     * @param string $state
     * @return string
     */
    public function vti_get_state($country_code, $state){        
        if($this->states[$country_code][$state]){
            return $this->states[$country_code][$state];
        }else{
            return $state;
        }
    }
    
    /**
     * Get country if available
     * @param string $country
     * @return array
     */
    public function vti_get_country($country){
        if (array_key_exists($country, $this->country_match)){
            $country_code = $this->country_match[$country];
            $country_name = $this->countries[$country_code];
            return array('country_code' => $country_code, 'country_name' => $country_name);
        }
    }
    
    /**
     * Insert Wordpress Post
     * Insert Wordpress Categories if Available
     * @param Array $data
     * @param Array $fields
     * @param String $author
     * @param String $post_type
     * @param String $taxonomy
     * @return Integer
     */
    public function vti_insert_wp_post_category($data, $fields, $author, $post_type, $taxonomy = null, $tags_taxonomy = null, $language_taxonomy = null, $tags = null) {
        if ($categoty = $data[$fields['category']]) {

            $cat_id = null;
            $tag_id = null;
            $tag_ids = array();

            $terms = get_terms(array(
                'taxonomy' => $taxonomy,
                'hide_empty' => false,
            ));
            
            foreach ((array)$terms as $term) {
                if ($_POST['vti_import_for'] == 'planners') {
                    if (strpos(strtolower($categoty), strtolower($term->name)) !== false) {
                        $cat_id[] = $term->term_id;
                    }
                } else {
                    if ($term->name == $categoty) {
                        $cat_id = $term->term_id;
                    }
                }
            }

            if (!$cat_id && $_POST['vti_import_for'] <> 'planners') {
                $category_data = array(
                    'taxonomy' => $taxonomy,
                    'cat_name' => $data[$fields['category']],
                );
                $cat_id = wp_insert_category($category_data);
            }
        }

        $post_data = array(
            'post_title' => $data[$fields['title']],
            'post_content' => $data[$fields['content']],
            'post_status' => 'publish',
            'post_author' => $author,
            'post_excerpt' => $data[$fields['excerpt']],
            'post_type' => $post_type
        );

        $post_id = wp_insert_post($post_data);

        if ($cat_id) {            
            if (is_string($cat_id)) {
                $cat_ids = array($cat_id);
            } else {
                $cat_ids = $cat_id;
            }
            wp_set_post_terms($post_id, $cat_ids, $taxonomy);
        }

        if ($language_taxonomy) {
            $languages = get_terms(array(
                'taxonomy' => $language_taxonomy,
                'hide_empty' => false,
            ));

            if ($country_language[$data[$fields['venue_country']]]) {
                $language_name = trim($country_language[$data[$fields['venue_country']]]);
            } else {
                $language_name = "English";
            }
            
            foreach ((array)$languages as $language) {
                if ($language->name == $language_name) {
                    $lang_id = $language->term_id;
                }
            }

            if (!$lang_id) {                
                $language_data = array(
                    'taxonomy' => $language_taxonomy,
                    'cat_name' => $language_name,
                );
                $lang_id = wp_insert_category($language_data);
            }

            wp_set_post_terms($post_id, array($lang_id), $language_taxonomy);
        }

        if ($tags_taxonomy) {
            $tag_terms = get_terms(array(
                'taxonomy' => $tags_taxonomy,
                'hide_empty' => false,
            ));

            foreach ($tags as $tag => $index) {
                if ($data[$index] && strtolower($data[$index]) == "yes") {
                    foreach ((array)$tag_terms as $tag_term) {
                        if ($tag_term->name == $tag) {
                            $tag_id = $tag_term->term_id;
                        }
                    }

                    if (!$tag_id) {
                        $tag_data = array(
                            'taxonomy' => $tags_taxonomy,
                            'cat_name' => $tag,
                        );
                        $tag_id = wp_insert_category($tag_data);
                    }
                    $tag_ids[] = $tag_id;
                    $tag_id = null;
                }
            }

            if (count($tag_ids) > 0) {
                wp_set_post_terms($post_id, $tag_ids, $tags_taxonomy);
            }
        }

        return $post_id;
    }

    /**
     * Get Working Days by working hours string
     * @param String $days_open
     * @return String
     */
    public function vti_get_open_days($days_open) {
        $explode_days = explode(",", $days_open);

        foreach ($explode_days as $explode_day) {
            $day_hours = explode(":", $explode_day);
            if (strpos(strtolower($day_hours[1]), 'closed') === false) {
                $days[] = $day_hours[0];
            }
        }
        if ($days) {
            return $days;
        } else {
            return false;
        }
    }

}
