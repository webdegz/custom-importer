<?php

$this->planner = array(
    'data' => array(
        'vti_import_id' => 4,
        'title' => 5,
        'excerpt' => 3,
        'content' => 33,
        'category' => 35,
        'address' => 6,
        'address2' => 7,
        'country' => 2,
        'city' => 8,
        'state' => 9,
        'zip' => 10,
        'geo_location_latitude' => 29,
        'geo_location_longitude' => 30,
        'contact_name' => 12,
        'email_address' => 20,
        'phone' => 14,
        'website' => 22,
    ),
    'images' => array(
        'vti_import_id' => 0,
        'file_name' => 3
    ),
    'data_required' => array(
        'vti_import_id', 'title', 'content', 'category', 'address', 'city', 'state', 'zip', 'country', 'geo_location_latitude', 'geo_location_longitude'
    ),
    'images_required' => array(
        'vti_import_id', 'file_name'
    ),
    'country_language' => array(
        'Portugal' => 'Portuguese',
        'USA' => 'English',
        'Canada' => 'English'
    )
);

$this->venue = array(
    'data' => array(
        'vti_import_id' => 0,
        'title' => 2,
        'excerpt' => 3,
        'content' => 5,
        'category' => 6,
        'venue_address' => 14,
        'venue_city' => 15,
        'venue_state' => 16,
        'venue_zip' => 17,
        'venue_country' => 18,
        'geo_location_latitude' => 20,
        'geo_location_longitude' => 21,
        'venue_phone' => 97,
        'website_address' => 100,
        'days_open' => 102,
        'meeting_rooms' => 12,
        'sleeping_rooms' => 11,
        'largest_event_room' => 22,
        'smallest_event_room' => 23,
        'event_rooms_capacity' => 24,
        'wi_fi_capability' => 44,
        'outside_suppliers_permitted' => 91
    ),
    'images' => array(
        'vti_import_id' => 0,
        'file_name' => 3
    ),
    'attachments' => array(
        'vti_import_id' => 0,
        'title' => 3,
        'file_name' => 4,
    ),
    'data_required' => array(
        'vti_import_id', 'title', 'content', 'category', 'venue_address', 'venue_city', 'venue_state', 'venue_zip', 'venue_country', 'geo_location_latitude', 'geo_location_longitude'
    ),
    'images_required' => array(
        'vti_import_id', 'file_name'
    ),
    'attachment_required' => array(
        'vti_import_id', 'file_name'
    ),
    'venue_tags' => array(
        'Laundry' => 40,
        'Voice Mail Box' => 41,
        'Luggage_Storage' => 42,
        'Urban Views' => 43,
        'Internet' => 44,
        'Concierge Services' => 45,
        'Room Service' => 46,
        'Restaurant on Site' => 47,
        'Onsite Catering' => 48,
        'Wheelchair Accessible' => 49,
        'Rental Car Services' => 50,
        'Security Staff Onsite' => 51,
        'Bus Parking Available' => 53,
        'Paid Parking Available' => 54,
        'Business Center' => 55,
        'Audio/Video Capabilities' => 56,
        'Video Conferencing' => 57,
        'VIP Services' => 58,
        'Health Club' => 59,
        'Indoor Pool' => 60,
        'Train' => 61,
        'Bus' => 62,
        'Subway' => 63,
        'Taxi' => 64,
        'Portable Walls' => 65,
        'Staging' => 66,
        'Piano' => 67,
        'Dance Floors' => 68,
        'Free Local Calls' => 69,
        'Free Toll Free Calls' => 70,
        'Gift Shop on Site' => 71,
        'Street Parking' => 72,
        'Valet Parking Available' => 73,
        'Whirlpool' => 74,
        'Shuttle' => 75,
        'Spa/Salon' => 76,
        'Pets Welcome' => 77,
        'Loading Dock Available' => 78,
        'Mountain Views' => 79,
        'Free Parking' => 80,
        'Outdoor Pool' => 81,
        'Tennis' => 82,
        'Golf on Grounds' => 83,
        'Portable Heaters' => 84,
        'Ocean/Water Views' => 85,
        'Private Space' => 86,
        'Garden Views' => 87,
        'Semi Private Space' => 88,
        'Skiing' => 89,
        'Outdoor Space' => 90,
        'Outside Caterers Allowed' => 91,
        'Casino' => 92,
        'Water Sports' => 93,
        'Free Airport Shuttle' => 94,
    ),
    'country_language' => array(
        'Portugal' => 'Portuguese',
        'USA' => 'English',
        'Canada' => 'English'
    )
);
