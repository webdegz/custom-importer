<?php
/*
  Plugin Name: VenueTown Importer
  Plugin URI: http://codup.io/
  Description: Import Venues and planners
  Version: 1.1.1.2
  Author Name: Codup
  Author URI: http://codup.io/
 */
if (!defined('DS')) {
    define("DS", DIRECTORY_SEPARATOR);
}
if (!defined('VERSION')) {
    define("VERSION", '1.1.1.2');
}

include(dirname(__FILE__) . DS . 'includes' . DS . 'class.importer.php');
new VenueTown_Importer();